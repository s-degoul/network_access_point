#!/usr/bin/env python3
#-*- coding:utf-8 -*-

import sys
import os
import subprocess
import re
import time
import shutil

import config


class APUtils():
    """ Manage interactions with the system network commands to set up a network access point """

    def __init__(self):
        # list of available interfaces
        self.interfaces = self.__get_network_interfaces()
        # intf_internet: str. Network interface for connection to the Internet
        self.intf_internet = ""
        # intf_subnet: str. Network interface for connection to the LAN
        self.intf_subnet = ""
        # subnet_conf: dict. Configuration parameters for the subnet
        #     subnet: str. IP address characterizing the subnet
        #     netmask: str. Netmask of the subnet
        #     router_ip: str. IP address of the router in the subnet
        #     max_client_number: int. Maximum number of client in the subnet
        self.subnet_conf = dict(subnet="",
                                netmask="",
                                router_ip="",
                                max_client_number=0)
        # dhcp_server_conf: dict. Configuration parameters for DHCP server
        #     conf_file: str. Path of the DHCP server's configuration file
        #     dns_ip: str or tuple. IP address(es) of domain name servers
        #     min_client_ip: str. Minimal IP address for DHCP clients
        self.dhcp_server_conf = dict(conf_file="",
                                     dns_ip="",
                                     min_client_ip="")
        # dhcp_server_process: subprocess.Popen or None. DHCP server process
        self.dhcp_server_process = None
        # hostapd_conf: dict. Configuration parameters for hostapd utility
        #     conf_file: str. Path of the hostapd utility's configuration file
        #     ssid: str. SSID of the new WIFI network
        #     passphrase: str. WPA2 passphrase
        self.hostapd_conf = dict(conf_file="",
                                 ssid="",
                                 passphrase="")
        # hostapd_process: subprocess.Popen or None. Hostapd process
        self.hostapd_process = None

    def set_intf_internet(self, intf):
        """ Set the value of self.intf_internet
        Input: intf: str. Name of the interface
        Output: None
        """
        if self.intf_subnet and intf == self.intf_subnet:
            raise ValueError(
                "The network interface connected to the Internet must not be the same than the interface connected to the LAN")
        if intf not in self.interfaces:
            raise ValueError("The network interface " + intf +
                             " is not a valid one for the interface connected to the Internet")
        self.intf_internet = intf

    def set_intf_subnet(self, intf):
        """ Set the value of self.intf_subnet
        Input: intf: str. Name of the interface
        Output: None
        """
        if self.intf_internet and intf == self.intf_internet:
            raise ValueError(
                "The network interface connected to the LAN must not be the same than the interface connected to the Internet")
        if intf not in self.interfaces:
            raise ValueError("The network interface " + intf +
                             " is not a valid one for the interface connected to the LAN")
        self.intf_subnet = intf

    def set_max_client_number(self, number):
        """ Set the value of self.subnet_conf["max_client_number"]
        Input: number: int. Maximum number of client in the subnet
        Output: None
        """
        assert number > 0, "set_max_client_number(): maximum number of client in the subnet must be a positive integer"

        self.subnet_conf["max_client_number"] = number

    def set_subnet_conf(self, subnet_conf):
        """ Set values of self.subnet_conf dict
        (max_client_number optional if already specifically defined by set_max_client_number function)
        Input: subnet_conf: dict with following keys:
                   subnet: str. IP address characterizing the subnet
                   netmask: str. Netmask of the subnet
                   router_ip: str. IP address of the router in the subnet
        Output: None
        """
        for param, value in subnet_conf.items():
            self.subnet_conf[param] = value
        # TODO: check if netmask, subnet and router_ip addresses are coherent
        if False in [bool(v) for k, v in self.subnet_conf.items() if k in ("subnet_conf", "subnet", "router_ip")]:
            raise ValueError(
                "set_subnet_conf(): configuration parameters must include subnet, netmask and router_ip")

    def set_dhcp_server_conf(self, dhcp_server_conf):
        """ Set values of self.dhcp_server_conf
        Input: dhcp_server_conf: dict with following keys:
                   conf_file: str. Path of the DHCP server's configuration file
                   dns_ip: str or tuple, optional. IP address of domain name servers
                   min_client_ip: str, optional. Minimal IP address for DHCP clients
        Output: None
        """
        assert False not in [i in dhcp_server_conf.keys() for i in ("conf_file", "dns_ip", "min_client_ip")
                             ], "set_dhcp_server_conf(): DHCP server configuration parameters must include conf_file, dns_ip and min_client_ip"

        self.dhcp_server_conf = dhcp_server_conf
        if not self.dhcp_server_conf["conf_file"]:
            raise ValueError(
                "set_dhcp_server_conf(): conf_file parameter must be defined")

    def set_hostapd_conf(self, hostapd_conf):
        """ Set values of self.hostapd_conf
        Input: hostapd_conf: dict with following keys:
                   conf_file: str. Path of the hostapd utility's configuration file
                   ssid: str. SSID of the new WIFI network
                   passphrase: str. WPA2 passphrase
        Output: None
        """
        assert False not in [i in hostapd_conf.keys() for i in ("conf_file", "ssid", "passphrase")
                             ], "set_hostapd_conf(): Hostapd configuration parameters must include conf_file, ssid and passphrase"

        # TODO: check validity of ssid and wpa passphrase values -> specific functions
        self.hostapd_conf = hostapd_conf
        if False in [bool(v) for v in self.hostapd_conf.values()]:
            raise ValueError(
                "Hostapd configuration: all parameters must be defined")

    ###########
    # Generic #
    ###########

    @staticmethod
    def __print_err(*args, **kwargs):
        """ Print on standard error output stderr instead of stdout
        Input: same as print() function
        Output: None
        """
        print(*args, file=sys.stderr, **kwargs)

    @staticmethod
    def __run_process(command, use_shell=False, exit_if_exception=False, error_message="Error", error_code=1, print_error=True):
        """ Runs a sub-process, wait for termination and returns the process output (stdout).

        Input: command : str or list of str. First string is command, items are command-line options.
               use_shell: bool. If true, system shell will be used to run the program.
                          Otherwise, the program will be run directly with popen().
                          (Some program need to have a full shell environment in order to run properly.)
               exit_if_exception: bool. Should the program stop if the command returns an exception ?
               error_message: str. Error message to display if an exception arises
               error_code: int not null. Error code to return if an exception arises
               print_error: bool. Should error messages be printed out in the standard output ?
        Ouput: dict: output: str. The output of the commande (stdout) if any, or error_message if an exception occured
                     status: int. 0 if successfull, error_code if an exception occured
                     # Caution: if use_shell = True, pid of the spawning shell -> TODO: check
                     process: subprocess.Popen. Process launched

        Example:
            __run_process(['ifconfig','-a'])
            __run_process('DISPLAY=:0 su {} -c "scrot {}"'.format(user,
                        filepath), use_shell=True)
        """
        assert error_code != 0, "__run_process(): error_code must not be 0"
        try:
            if not use_shell:
                process = subprocess.run(command, stdout=subprocess.PIPE,
                                         stderr=subprocess.PIPE,  # shell=use_shell, ??
                                         universal_newlines=True)
                output = process.stdout
                err = process.stderr
                process.check_returncode()
            else:
                process = subprocess.Popen(
                    command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, executable="/bin/bash", universal_newlines=True)
                pc = process.communicate()
                output = pc[0]
                err = pc[1] if pc[1] else ""
                if process.returncode:
                    raise subprocess.CalledProcessError(
                        returncode=process.returncode, cmd=command)
            return dict(output=output, status=0, process=process)
        except OSError as e:
            if print_error:
                APUtils.__print_err(
                    error_message + "\nI/O error: " + str(e))
        except (ValueError, TypeError) as e:
            if print_error:
                APUtils.__print_err(
                    error_message + "\nInvalid argument: " + str(e))
        except subprocess.CalledProcessError as e:
            if print_error:
                APUtils.__print_err(error_message + "\nSystem returns: " +
                                    str(err) + "\n" + str(output) + "\n" + str(e))

        if exit_if_exception:
            print("An error occurred. Aborting.")
            sys.exit(error_code)
        else:
            return dict(output=error_message, status=error_code, process=None)

    # @staticmethod
    # def __is_service_active(service):
    #     """ Is a systemd service active ?
    #     Input: service: str. Name of the systemd service
    #     Ouput: bool
    #     """
    #     return APUtils.__run_process(["systemctl", "is-active", service], exit_if_exception=False,
    #                                  error_message="", print_error=False)["status"] == 0

    #########################
    # Interfaces management #
    #########################

    def __get_network_interfaces(self):
        """ Get name and type of available Ethernet or WIFI interfaces
        Input: None
        Ouput: dict: key=name, value=type
        """
        interfaces = {}
        for intf in os.listdir("/sys/class/net"):
            interfaces[intf] = APUtils.__run_process(command=["./get_intf_type.sh", intf],
                                                     error_message="Unable to determine type of network interface " + intf)["output"].strip()
        return {k: v for k, v in interfaces.items() if v.lower() in ("ethernet", "wifi")}

    def is_wifi_interface(self, intf):
        """ Is the given network interface a WIFI interface ?
        Input: intf: str. Name of the interface
        Output: bool
        """
        assert isinstance(intf, str) and len(
            intf) > 0 and intf in self.interfaces,\
            "is_wifi_interface(): no valid name of network interface provided"

        # return intf in APUtils.__run_process(["iw", "dev"], print_error=False)["output"]
        return self.interfaces[intf].lower() == "wifi"

    # def scan_wifi(self, intf):
    #     scan = APUtils.__run_process(command=["iw", "dev", intf, "scan"],
    #                        error_message="Error in scanning wifi network")

    #     print(parse_scan(scan))

    def activate_intf(self, intf):
        """ Enable a network interface
        Input: intf: str. Name of the interface
        Output: None
        """
        try_number = 0
        modules = list(config.intf_modules["wifi"]) if self.is_wifi_interface(intf)\
            else list(config.intf_modules["ethernet"])
        while APUtils.__run_process(command=["ip", "link", "set", intf, "up"],
                                    error_message="Cannot activate network interface " + intf)["status"] != 0:
            try_number += 1
            if try_number > 1:
                APUtils.__print_err("Cannot activate network interface " +
                                    intf + ". Aborting")
                sys.exit(1)
            print("Reloading kernel modules for interface " + intf)
            APUtils.__run_process(command=["modprobe", "-r"] + modules,
                                  error_message="Cannot stop kernel network modules: " +
                                  ", ".join(modules),
                                  exit_if_exception=True)
            time.sleep(3)       # usefull ?
            APUtils.__run_process(command=["modprobe"] + modules,
                                  error_message="Cannot start kernel network modules: " +
                                  ", ".join(modules),
                                  exit_if_exception=True)

    def desactivate_intf(self, intf):
        """ Disable a network interface
        Input: intf: str. Name of the interface
        Output: None
        """
        APUtils.__run_process(command=["ip", "link", "set", intf, "down"],
                              error_message="Cannot disable network interface " + intf)

    ##############
    # Connection #
    ##############

    @staticmethod
    def __set_IP(intf, dhcp=True, ip_client="", ip_router="", broadcast=""):
        """ Set the IP adress of the given network interface
        Input: intf: str. Name of the interface
               dhcp: bool. Should DHCP protocle be used ?
               ip_client: str, optional. IP adress to apply (for static attribution). Must not be empty if dhcp is False
               ip_router: str, optional. IP adress of the router, for default route setting
               broadcast: str, optional. Broadcast address of the network
        Output: None

        """
        assert dhcp or ip_client,\
            "__set_IP(): no IP address given for client while dhcp is disabled"

        if dhcp:
            APUtils.__run_process(command=[config.dhcp_client_command, intf],
                                  error_message="Cannot get IP address with DHCP on interface " + intf)
        else:
            ip_command = ["ip", "addr", "add", ip_client, "dev", intf]
            if broadcast:
                ip_command[4:4] = ["broadcast", broadcast]
            APUtils.__run_process(command=ip_command,
                                  error_message="Cannot get static IP address on interface " + intf)
            if ip_router:
                APUtils.__run_process(command=["ip", "route", "add", "default", "via", ip_router],
                                      error_message="Cannot set default route via router " + ip_router)

    @staticmethod
    def remove_IP(intf, dhcp=True, route=True):
        """ Remove IP configuration of the given network interface. Stop the DHCP client if any, else erase manual configuration
        Input: intf: str. Name of the interface
               dhcp: bool. Should stopping a dhcp client be attempted ?
               route: bool. Should route configuration be erased ?
        Output: None
        """
        if not dhcp or APUtils.__run_process([config.dhcp_client_command, "-k", intf], print_error=False)["status"] != 0:
            APUtils.__run_process(["ip", "addr", "flush", "dev", intf])
            if route:
                APUtils.__run_process(
                    ["ip", "route", "flush", "dev", intf])

    def connect_wifi(self, ssid, encryption="", passphrase="", **ip_config):
        """" Connect the given interface to a WIFI network
        Input: intf: str. Name of the interface
               encryption: str. Type of WIFI encryption. Must be one of: "", "wep", "wpa"
               passphrase: str. Passphrase for encryption
               ip_config: arguments for the function __set_IP()
        Output: None
        """
        assert encryption in ("", "wpa", "wep"),\
            "connect_wifi(): unknown wifi encryption type " + encryption

        # Activation of the network interface
        self.activate_intf(self.intf_internet)
        # Disconnect interface if already connected
        self.disconnect(self.intf_internet)
        # TODO: Check if DHCP already active for this interface?

        # Connection to the wifi network with SSID = ssid
        if not encryption:
            APUtils.__run_process(command=["iw", "dev", self.intf_internet, "connect", ssid],
                                  error_message="Cannot connect to wifi network " + ssid + " without encryption")
        elif encryption == "wep":
            APUtils.__run_process(command=["iw", "dev", self.intf_internet,
                                           "connect", ssid, "key", "0:" + passphrase],
                                  error_message="Cannot connect to wifi network " + ssid + " with given WEP encryption key")
        elif encryption == "wpa":
            # Creating a temporary file with wpa_supplicant configuration
            temp_wpa_supplicant_file = "/tmp/wpa_supplicant.conf"
            wpa_supplicant_conf = APUtils.__run_process(
                ["/usr/bin/wpa_passphrase", ssid, passphrase])["output"]
            # Removing unencrypted PSK passphrase
            wpa_supplicant_conf = "".join(
                re.split(r"\t#psk=.*\n", wpa_supplicant_conf))
            with open(temp_wpa_supplicant_file, "w", encoding="utf-8") as f:
                f.write(wpa_supplicant_conf)

            APUtils.__run_process(command=["wpa_supplicant", "-B", "-i", self.intf_internet, "-c", temp_wpa_supplicant_file],
                                  error_message="Cannot connect to wifi network " + ssid + " with given WPA encryption key")
            # Removing temporary file with wpa_supplicant configuration
            os.remove(temp_wpa_supplicant_file)

        # Setting an IP address
        try:
            APUtils.__set_IP(self.intf_internet, **ip_config)
        except TypeError as e:
            APUtils.__print_err(e)
            return False

    def connect_ethernet(self, **ip_config):
        """" Connect the given interface to an ethernet network
        Input: ip_config: dict. Arguments for the function __set_IP()
        Output: None
        """
        # Activation of the network interface
        self.activate_intf(self.intf_internet)
        # Disconnect interface if already connected
        self.disconnect(self.intf_internet)

        # Getting an IP address
        try:
            APUtils.__set_IP(self.intf_internet, **ip_config)
        except TypeError as e:
            APUtils.__print_err(e)
            return False

    @staticmethod
    def is_connected(intf):
        """ Is the given interface connected to a network ?
        Input: intf: str. Name of the interface
        Output: bool
        """
        # return "Connected to" in APUtils.__run_process(["iw", "dev", intf, "link"])["output"]
        return "state UP" in APUtils.__run_process(["ip", "link", "ls", "dev", intf])["output"]

    def disconnect(self, intf, desactivate_intf=False):
        """ Disconnect the given interface
        Input: intf: str. Name of the interface
               desactivate_intf: bool. Should the interface be desactivated?
        Output: None
        """
        # Removing IP configuration
        APUtils.remove_IP(intf)
        # Stopping wpa_supplicant process already active for this WIFI interface
        if self.is_wifi_interface(intf):
            APUtils.__run_process(command=["wpa_cli", "-i", intf, "terminate"],
                                  error_message="Cannot stop wpa_supplicant for the interface " + intf)
            # wpa_processes = APUtils.__run_process("ps -ef | grep wpa_supplicant | grep " + intf,
            #                             use_shell=True)["output"].split("\n")
            # wpa_pids = [p.strip().split()[1]
            #             for p in wpa_processes if p and not "grep" in p]
            # # print(wpa_pids)
            # if len(wpa_pids) > 0:
            #     APUtils.__run_process(["kill"] + wpa_pids)
        # Disabling interface
        if desactivate_intf:
            self.desactivate_intf(intf)

    #######
    # NAT #
    #######

    def __enable_packet_forwarding(self, intf_subnet_only=False):
        """ Enable the packet forwarding, globally or for one given interface
        Input: intf_subnet_only: bool. Should the packet forwarding be limited for the subnet interface?
        Output: None
        """
        if not intf_subnet_only:
            if APUtils.__run_process(["sysctl", "-n", "net.ipv4.ip_forward"])["output"].strip() != "1":
                APUtils.__run_process(
                    ["sysctl", "net.ipv4.ip_forward=1"])
        else:
            # enable forwarding selectively for interface self.intf_subnet ?
            pass                    # TODO

    def create_NAT(self):
        """ Set up a NAT
        Input: None
        Output: None
        """
        self.__enable_packet_forwarding()  # TODO: selectively for interface intf_subnet ?

        # Converting source IP from local to public (router) IP
        APUtils.__run_process(["iptables", "-t", "nat", "-A", "POSTROUTING",
                               "-o", self.intf_internet, "-j", "MASQUERADE"])
        # Accept forwarding from established connections
        APUtils.__run_process(["iptables", "-A", "FORWARD", "-m", "conntrack",
                               "--ctstate", "RELATED,ESTABLISHED", "-j", "ACCEPT"])
        # Forwarding packets from intf_subnet interface to intf_internet interface
        APUtils.__run_process(["iptables", "-A", "FORWARD", "-i", self.intf_subnet,
                               "-o", self.intf_internet, "-j", "ACCEPT"])

    def delete_NAT(self):  # TODO
        # removing related iptables rules
        pass

    ###############
    # DHCP server #
    ###############

    @staticmethod
    def is_ip_valid(ip):
        """ Is the format of the given IP address valid ?
        Input: ip: str. IPv4 address
        Output: bool
        """
        assert isinstance(
            ip, str), "is_ip_valid(): address must be a string"

        patern = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
        if not patern.match(ip):
            return False

        parts = [int(j) for j in ip.split(".")]
        return False not in [i >= 0 and i < 256 for i in parts]

    @staticmethod
    def __get_broadcast(subnet):      # TODO: use netmask?
        """ Get broadcast address from a subnet address
        Input: subnet: str. IP address characterizing the subnet
        Output: str
        """
        assert APUtils.is_ip_valid(
            subnet), "__get_broadcast(): subnet must be a valid IP address"

        # TODO: .255 instead of .0?
        return ".".join(subnet.split(".")[:3]) + ".0"

    @staticmethod
    def __extend_binary(bin_seq, length):
        """ Fill the left part of a binary sequence with 0 to obtain a length of "length" digits
        Input: bin_seq: str. A binary sequence (like a bin without "0b" at the beginning)
               length: int. Desired length
        Output: str
        """
        if len(bin_seq) > length:
            raise ValueError(
                "__extend_binary(): length of given sequence is greated than given length")
        return '0' * (length - len(bin_seq)) + bin_seq

    @staticmethod
    def __convert_ip_to_binary(ip):
        """ Compute binary form of the given ip address
        Input: ip: str. IP address
        Output: str
        """
        assert APUtils.is_ip_valid(
            ip), "__convert_ip_to_binary(): given ip is not valid"

        # TODO: it is not a bin but a str!
        return "0b" + "".join([APUtils.__extend_binary(bin(int(i))[2:], 8) for i in ip.split(".")])

    @staticmethod
    def __convert_ip_to_num(ip):
        """ Compute the numeric value of the given ip address
        Input: ip: str. IP address
        Output: int
        """
        return int(APUtils.__convert_ip_to_binary(ip), 2)

    @staticmethod
    def __convert_ip_num_to_classic(ip_num):
        """ Convert a given ip in numeric form into the classic form (4 numbers)
        Input: int. IP address in numeric form
        Output: str
        """
        ip_bin = APUtils.__extend_binary(bin(ip_num)[2:], 32)
        return ".".join([str(i) for i in [int(ip_bin[:8], 2), int(ip_bin[8:16], 2), int(ip_bin[16:24], 2), int(ip_bin[24:], 2)]])

    @staticmethod
    def convert_netmask_to_num(netmask):
        """ Convert a netmask from a 4 numbers form to numeric
        Input: str. Netmask
        Output: int
        """
        netmask_bin = APUtils.__convert_ip_to_binary(netmask)
        if not re.compile("^1*0*$").match(netmask_bin[2:]):
            raise ValueError(
                "convert_netmask_to_num(): given netmask is not valid")

        return netmask_bin[2:].count("1")

    def __compute_subnet_range(self):
        # TODO: use netmask too
        """ Compute range of IP addresses for a subnet
        Input: None
        Output: tuple of two IP addresses (first and last)
        """
        subnet_num = APUtils.__convert_ip_to_num(
            self.subnet_conf["subnet"])
        router_ip_num = APUtils.__convert_ip_to_num(
            self.subnet_conf["router_ip"])
        max_ip_num = subnet_num + int("0b" + ("1" * (32 - APUtils.convert_netmask_to_num(
            self.subnet_conf["netmask"]))), 2)

        if self.dhcp_server_conf["min_client_ip"]:
            min_client_ip_num = APUtils.__convert_ip_to_num(
                self.dhcp_server_conf["min_client_ip"])
            max_client_ip_num = min_client_ip_num + \
                self.subnet_conf["max_client_number"]
            # Problem if IP address of the router between minimal and maximal addresses for the clients
            if router_ip_num > min_client_ip_num and router_ip_num < max_client_ip_num:
                raise ValueError(
                    "__compute_subnet_range(): max_client_number of addresses can not be reached given IP addresses of router and choosen minimal IP address")
        else:
            if (subnet_num + self.subnet_conf["max_client_number"]) < router_ip_num:
                min_client_ip_num = subnet_num + 1
            else:
                min_client_ip_num = router_ip_num + 1
            max_client_ip_num = min_client_ip_num + \
                self.subnet_conf["max_client_number"]
        if max_client_ip_num > max_ip_num:
            raise ValueError(
                "__compute_subnet_range(): maximal IP address for clients out of range")
        return (APUtils.__convert_ip_num_to_classic(min_client_ip_num),
                APUtils.__convert_ip_num_to_classic(max_client_ip_num))
        # except ValueError as e:
        #     APUtils.__print_err(
        #         "Unable to compute a subnet range with given parameters")
        #     return None

    def __setup_dhcp_config(self):
        """ Create the config file of the DHCP server
        Input: None
        Output: None
        """
        conf = "subnet " + self.subnet_conf["subnet"] + \
            " netmask " + self.subnet_conf["netmask"] + " {\n"
        conf += "  range " + " ".join(self.__compute_subnet_range()) + ";\n"
        conf += "  option domain-name-servers " + \
            (", ".join(self.dhcp_server_conf["dns_ip"]) if isinstance(
                self.dhcp_server_conf["dns_ip"], tuple) else self.dhcp_server_conf["dns_ip"]) + ";\n"
        conf += "  option routers " + \
            self.subnet_conf["router_ip"] + ";\n"
        conf += "  option broadcast-address " + \
            APUtils.__get_broadcast(self.subnet_conf["subnet"]) + ";\n}\n"

        existing_conf = ""
        try:
            with open(self.dhcp_server_conf["conf_file"], "r") as f:
                existing_conf = f.read()
        except (IOError, OSError) as e:
            APUtils.__print_err(
                "Unable to find the configuration file of DHCP server")
        if conf != existing_conf:
            try:
                with open(self.dhcp_server_conf["conf_file"], "w") as f:
                    f.write(conf)
                    print("Configuration of DHCP server updated")
                    return True
            except (IOError, OSError) as e:
                APUtils.__print_err(
                    "Unable to write the configuration file of DHCP server. Aborting.")
                sys.exit(1)
        else:
            print("Configuration of DHCP server already existing")
            return True

    def start_dhcp_server(self):
        """ Start a DHCP server for the given interface
        Input: None
        Output: None
        """
        self.__setup_dhcp_config()

        # Attributing IP address to network interface
        APUtils.__set_IP(intf=self.intf_subnet,
                         dhcp=False,
                         ip_client=self.subnet_conf["router_ip"] + "/" + str(
                             APUtils.convert_netmask_to_num(self.subnet_conf["netmask"])))

        # Starting DHCP server
        self.dhcp_server_process = APUtils.__run_process(command=[config.dhcp_server_command, "-4", "-q", "-cf", self.dhcp_server_conf["conf_file"], self.intf_subnet],
                                                         error_message="Cannot start DHCP server for interface " + self.intf_subnet)["process"]
        # Allowing DHCP requests from clients
        APUtils.__run_process(command=["iptables", "-I", "INPUT", "-p", "udp", "--dport", "67", "-i", self.intf_subnet, "-j", "ACCEPT"],
                              error_message="Cannot allow incoming connections to UDP port 67 for DHCP server")
        # Allowing DNS requests    TODO: in a separate function
        APUtils.__run_process(command=["iptables", "-I", "INPUT", "-p", "udp", "--dport", "53", "-s", self.subnet_conf["subnet"] + "/" + str(APUtils.convert_netmask_to_num(self.subnet_conf["netmask"])), "-j", "ACCEPT"],
                              error_message="Cannot allow incoming connections to UDP port 53 for DNS requests")
        APUtils.__run_process(command=["iptables", "-I", "INPUT", "-p", "tcp", "--dport", "53", "-s", self.subnet_conf["subnet"] + "/" + str(APUtils.convert_netmask_to_num(self.subnet_conf["netmask"])), "-j", "ACCEPT"],
                              error_message="Cannot allow incoming connections to TCP port 53 for DNS requests")

    def stop_dhcp_server(self):
        """ Stop a DHCP server for the given interface
        Input: None
        Output: None
        """
        if isinstance(self.dhcp_server_process, subprocess.Popen):
            self.dhcp_server_process.terminate()
        else:
            dhcp_server_processes = APUtils.__run_process(command="ps -ef | grep dhcpd.*" + self.intf_subnet,
                                                          use_shell=True)["output"].strip().split("\n")
            dhcp_server_pids = [p.strip().split()[1]
                                for p in dhcp_server_processes if not "grep" in p]
            if len(dhcp_server_pids) > 0:
                APUtils.__run_process(command=["kill"] + dhcp_server_pids)
            else:
                print("DHCP server already inactive for interface " +
                      self.intf_subnet)
        # TODO: remove related iptables rules

    ###########
    # Hostapd #
    ###########

    def __setup_hostapd_config(self):
        """ Create the configuration file of hostapd utility
        Input: None
        Output: None
        """
        try:
            shutil.copyfile(
                "hostapd.conf_model", self.hostapd_conf["conf_file"])
        except (OSError, IOError) as e:
            APUtils.__print_err("Unable to copy hostapd configuration file in " +
                                self.hostapd_conf["conf_file"])
        # params = copy.copy(locals())
        # for p in params.keys():
        for param, value in dict(intf_wifi=self.intf_subnet,
                                 max_client_number=self.subnet_conf["max_client_number"],
                                 ssid=self.hostapd_conf["ssid"],
                                 passphrase=self.hostapd_conf["passphrase"]).items():

            APUtils.__run_process("sed -i 's/<" + str(param) + ">/" + str(value) + "/g' " + self.hostapd_conf["conf_file"],
                                  error_message="Unable to change the parameter " +
                                  param + " in the hostapd configuration file",
                                  use_shell=True)

    def start_hostapd(self):
        """ Start the hostapd utility
        Input: None
        Output: None
        """
        self.__setup_hostapd_config()
        self.hostapd_process = APUtils.__run_process(command=[config.hostapd_command, "-B", self.hostapd_conf["conf_file"]],
                                                     error_message="Cannot start hostapd utility")["process"]

    def stop_hostapd(self):
        """ Stop the hostapd utility
        Input: None
        Output: None
        """
        if isinstance(self.hostapd_process, subprocess.Popen):
            self.hostapd_process.terminate()
        else:
            hostapd_processes = APUtils.__run_process(command="ps -ef | grep hostapd",
                                                      use_shell=True)["output"].strip().split("\n")
            hostapd_pids = [p.strip().split()[1]
                            for p in hostapd_processes if not "grep" in p]
            if len(hostapd_pids) > 0:
                APUtils.__run_process(command=["kill"] + hostapd_pids)
            else:
                print("Hostapd utility already inactive")
            # BUT DON'T KILL AN ALREADY ACTIVE HOSTAPD UTILITY!
