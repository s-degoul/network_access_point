# Command for the DHCP client
dhcp_client_command = "/sbin/dhclient"
# dhcp_client_command = "dhcpcd"

# Command for the DHCP server
dhcp_server_command = "/usr/sbin/dhcpd"

# Command for the hostapd utility
hostapd_command = "/usr/sbin/hostapd"

# Kernel modules managing network interfaces
intf_modules = dict(wifi=("iwldvm", "iwlwifi"),
                    ethernet=("e1000e",))

# Path (absolute or relative) to the configuration file of the DHCP server
dhcp_server_conf_file = "dhcpd.conf"

# Path (absolute or relative) to the configuration file of hostapd utility
hostapd_conf_file = "hostapd.conf"

# Default configuration parameters for the subnet
default_subnet_conf = dict(subnet="192.168.0.0",
                           netmask="255.255.255.0",
                           router_ip="192.168.0.1",
                           max_client_number=50)

# Default configuration parameters for the DHCP server
default_dhcp_server_conf = dict(dns_ip=("8.8.8.8", "8.8.4.4"),
                                min_client_ip="")  # should be empty here, to allow empty value for min_client_ip later
