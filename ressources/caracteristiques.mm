<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Configuration routeur pour point d&apos;acc&#xe8;s au r&#xe9;seau" FOLDED="false" ID="ID_544949009" CREATED="1508485464587" MODIFIED="1516872822083" STYLE="oval">
<font SIZE="18" BOLD="true"/>
<hook NAME="MapStyle" zoom="0.909">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_note_icons="true" fit_to_viewport="false;"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
<edge COLOR="#ff0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
<edge COLOR="#0000ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
<edge COLOR="#00ff00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
<edge COLOR="#ff00ff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5">
<edge COLOR="#00ffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6">
<edge COLOR="#7c0000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7">
<edge COLOR="#00007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8">
<edge COLOR="#007c00"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9">
<edge COLOR="#7c007c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10">
<edge COLOR="#007c7c"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11">
<edge COLOR="#7c7c00"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="10" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Principe" POSITION="right" ID="ID_604910885" CREATED="1516826470663" MODIFIED="1516829841533">
<edge COLOR="#007c00"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="configurer un ordinateur qui servira de routeur entre un r&#xe9;seau publique et un r&#xe9;seau local" ID="ID_1107950276" CREATED="1516826481615" MODIFIED="1516826534000"/>
<node TEXT="quelques soient les interfaces r&#xe9;seaux choisies" ID="ID_105330224" CREATED="1516826541694" MODIFIED="1516826563767"/>
<node TEXT="appellations" ID="ID_642341068" CREATED="1516825395472" MODIFIED="1520013474925">
<icon BUILTIN="list"/>
<node TEXT="interfaces" ID="ID_694728258" CREATED="1520013477227" MODIFIED="1520013479215">
<node TEXT="internet0" ID="ID_1390479142" CREATED="1516825404872" MODIFIED="1516826860289">
<font ITALIC="true"/>
<node TEXT="vers le r&#xe9;seau public" ID="ID_50017626" CREATED="1516825409051" MODIFIED="1516825409785"/>
</node>
<node TEXT="subnet0" ID="ID_1586753600" CREATED="1516825416920" MODIFIED="1516827022687">
<font ITALIC="true"/>
<node TEXT="vers le r&#xe9;seau local" ID="ID_392735554" CREATED="1516825420443" MODIFIED="1516825422065"/>
</node>
</node>
</node>
</node>
<node TEXT="Mat&#xe9;riel" POSITION="right" ID="ID_1119468483" CREATED="1516826066686" MODIFIED="1516829845667">
<edge COLOR="#7c0000"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="nano-ordinateur" ID="ID_271172147" CREATED="1516831314385" MODIFIED="1516831316686">
<node TEXT="Raspberry Pi" ID="ID_421045822" CREATED="1516831317377" MODIFIED="1516831322815"/>
<node TEXT="..." ID="ID_374310618" CREATED="1516831324057" MODIFIED="1516831325345"/>
</node>
<node TEXT="syst&#xe8;me" ID="ID_1582545784" CREATED="1516826069090" MODIFIED="1516831313975">
<node TEXT="interfaces r&#xe9;seaux x 2" ID="ID_428748887" CREATED="1516826285922" MODIFIED="1516826354013">
<node TEXT="1 Ethernet" ID="ID_1992160042" CREATED="1516826302258" MODIFIED="1516826305468"/>
<node TEXT="1 Wifi" ID="ID_1080547570" CREATED="1516826305986" MODIFIED="1516826328099">
<node TEXT="doit supporter mode AP (iw list)" ID="ID_229034567" CREATED="1522526960372" MODIFIED="1522526981363"/>
</node>
</node>
<node TEXT="distribution Linux" ID="ID_989590435" CREATED="1516826098375" MODIFIED="1516826109630"/>
<node TEXT="Python" ID="ID_276887013" CREATED="1516831331161" MODIFIED="1516831336337"/>
<node TEXT="services" ID="ID_1847273160" CREATED="1516826124061" MODIFIED="1516826130951">
<node TEXT="systemd" ID="ID_9440047" CREATED="1516826109632" MODIFIED="1516826142325">
<icon BUILTIN="help"/>
<node TEXT="sauf si Devuan" ID="ID_2154520" CREATED="1516826143125" MODIFIED="1516826148183"/>
<node TEXT="mais faciliterait les choses" ID="ID_236269874" CREATED="1516826152325" MODIFIED="1516826169926"/>
</node>
<node TEXT="ip" ID="ID_675109356" CREATED="1516826132125" MODIFIED="1516826135487"/>
<node TEXT="iw" ID="ID_232097492" CREATED="1516826138285" MODIFIED="1516826139495"/>
<node TEXT="wpa_supplicant" ID="ID_1314703413" CREATED="1516956323016" MODIFIED="1516956328300"/>
<node TEXT="DHCPD" ID="ID_411608031" CREATED="1516826173540" MODIFIED="1516827274952">
<node TEXT="serveur DHCP" ID="ID_1543227635" CREATED="1516827260273" MODIFIED="1516827263438"/>
</node>
<node TEXT="SSHD" ID="ID_96599270" CREATED="1516826179212" MODIFIED="1516827271923">
<icon BUILTIN="help"/>
<node TEXT="serveur SSH" ID="ID_1116726770" CREATED="1516827264809" MODIFIED="1516827267875"/>
</node>
<node TEXT="HTTPD" ID="ID_1572295182" CREATED="1516826191476" MODIFIED="1516827282573">
<node TEXT="Apache" ID="ID_1631759772" CREATED="1516826194564" MODIFIED="1516826197326">
<node TEXT="peu probable" ID="ID_788387852" CREATED="1516826198044" MODIFIED="1516826200582">
<node TEXT="consomme beaucoup de ressources" ID="ID_1356105532" CREATED="1516826200876" MODIFIED="1516826209341"/>
</node>
</node>
<node TEXT="Nginx" ID="ID_986020274" CREATED="1516826210724" MODIFIED="1516826212373"/>
</node>
</node>
</node>
<node TEXT="switch" ID="ID_1296585411" CREATED="1516826232635" MODIFIED="1516826239275">
<node TEXT="si" ID="ID_1216549654" CREATED="1516826239278" MODIFIED="1516826254067">
<node TEXT="Wifi -&gt; Ethernet" ID="ID_746073043" CREATED="1516826254070" MODIFIED="1516826254973"/>
<node TEXT="+ n&#xe9;cessit&#xe9; de connecter &gt; 1 machine" ID="ID_1391799582" CREATED="1516826255491" MODIFIED="1520013544406"/>
</node>
</node>
<node TEXT="c&#xe2;bles Ethernet" ID="ID_1171028678" CREATED="1516826372105" MODIFIED="1516826378961">
<node TEXT="&gt; 1 si Wifi -&gt; Ethernet" ID="ID_691445756" CREATED="1516826378964" MODIFIED="1516826392186"/>
</node>
<node TEXT="au moins un ordinateur" ID="ID_1426309641" CREATED="1516826404152" MODIFIED="1516826410672">
<node TEXT="configuration" ID="ID_987861890" CREATED="1516826410675" MODIFIED="1516826416682"/>
</node>
</node>
<node TEXT="Proc&#xe9;dure" POSITION="left" ID="ID_253184982" CREATED="1516824755777" MODIFIED="1516829840051">
<edge COLOR="#ff00ff"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="&#xe9;tat initial" ID="ID_1141365985" CREATED="1516824441959" MODIFIED="1516827411103" HGAP_QUANTITY="13.250000022351742 pt" VSHIFT_QUANTITY="-19.49999941885473 pt">
<icon BUILTIN="neutral"/>
<node TEXT="" ID="ID_1677995346" CREATED="1516824845009" MODIFIED="1516824845011">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="adresse IP statique" ID="ID_1183957979" CREATED="1516824413605" MODIFIED="1516824768650"/>
<node TEXT="DHCPD" ID="ID_160161290" CREATED="1516824427478" MODIFIED="1516827255083"/>
<node TEXT="" ID="ID_163502456" CREATED="1516824845008" MODIFIED="1516824845009">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="limitation &#xe0; une interface" ID="ID_97609924" CREATED="1516824585556" MODIFIED="1516824607389">
<node TEXT="d&apos;embl&#xe9;e" ID="ID_129987192" CREATED="1516824608484" MODIFIED="1516827440024">
<icon BUILTIN="help"/>
<node TEXT="impose choix de l&apos;interface permettant l&apos;acc&#xe8;s &#xe0; la config" ID="ID_1829418028" CREATED="1516824661203" MODIFIED="1516824684764"/>
</node>
<node TEXT="secondairement en tous les cas" ID="ID_1202565614" CREATED="1516824617547" MODIFIED="1516827446072">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_309581642" STARTINCLINATION="-109;37;" ENDINCLINATION="88;-25;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
</node>
</node>
<node TEXT="+/- SSHD" ID="ID_1093429995" CREATED="1516824478405" MODIFIED="1516827250323">
<icon BUILTIN="help"/>
<node TEXT="pour d&#xe9;pannage" ID="ID_1075822861" CREATED="1516824705154" MODIFIED="1516824726870"/>
</node>
<node TEXT="HTTPD" ID="ID_1764040822" CREATED="1516828141323" MODIFIED="1516828143966"/>
</node>
<node TEXT="modification IP &amp; DHCPD selon configuration" ID="ID_309581642" CREATED="1516827188082" MODIFIED="1516827458906">
<icon BUILTIN="full-1"/>
<node ID="ID_75772842" CREATED="1516827333504" MODIFIED="1516827345834"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      application &#224; <i>subnet0</i>&#160;seulement
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="gestion DHCPD par systemd" ID="ID_1506738681" CREATED="1516827503649" MODIFIED="1516827556399">
<node TEXT="dhcpd4@.service" ID="ID_1867511697" CREATED="1516827535077" MODIFIED="1516827536919">
<node TEXT="il faut enable systemd-networkd-wait-online.service" ID="ID_379962935" CREATED="1522346675311" MODIFIED="1522346686814">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
</node>
<node TEXT="connexion r&#xe9;seau public" ID="ID_214066697" CREATED="1516825931865" MODIFIED="1516827466000">
<icon BUILTIN="full-2"/>
<node TEXT="internet0" ID="ID_180569182" CREATED="1516825954665" MODIFIED="1516827043428">
<font ITALIC="true"/>
</node>
<node TEXT="Wifi" ID="ID_1468304429" CREATED="1516827573268" MODIFIED="1516827600764">
<node TEXT="wpa-supplicant" ID="ID_1172614872" CREATED="1516827600767" MODIFIED="1516827610780">
<node TEXT="wpa-passphrase" ID="ID_128221694" CREATED="1516827610783" MODIFIED="1516827615342"/>
</node>
</node>
<node TEXT="+/- dhcpcd" ID="ID_1617333839" CREATED="1516827624055" MODIFIED="1516827647861"/>
</node>
<node ID="ID_1693507988" CREATED="1516824898407" MODIFIED="1516827473093">
<icon BUILTIN="full-3"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      autoriser le <i>packet forwarding</i>
    </p>
  </body>
</html>
</richcontent>
<node TEXT="globalement" ID="ID_1589099153" CREATED="1516824942942" MODIFIED="1516824945824">
<node TEXT="sysctl net.ipv4.ip_forward=1" LOCALIZED_STYLE_REF="defaultstyle.note" ID="ID_672043168" CREATED="1516824920807" MODIFIED="1516827805576">
<font SIZE="8"/>
</node>
</node>
<node ID="ID_745847384" CREATED="1516824933375" MODIFIED="1516827492803">
<icon BUILTIN="help"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      s&#233;lectivement pour <i>subnet0</i>
    </p>
  </body>
</html>
</richcontent>
<node TEXT="difficult&#xe9;s potentielles avec systemd" ID="ID_212961702" CREATED="1516827661571" MODIFIED="1516827682772">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
</node>
<node TEXT="configuration NAT" ID="ID_1548949676" CREATED="1516824989238" MODIFIED="1516827818722">
<icon BUILTIN="full-4"/>
<node TEXT="convertir IP source locale en IP publique" ID="ID_1142148640" CREATED="1516825015717" MODIFIED="1516825084544">
<node TEXT="iptables -t nat -A POSTROUTING -o internet0 -j MASQUERADE" ID="ID_1599064344" CREATED="1516825000406" MODIFIED="1516827804704">
<font SIZE="8"/>
</node>
</node>
<node TEXT="accepter redirection d&apos;une connexion &#xe9;tablie" ID="ID_930716055" CREATED="1516825100404" MODIFIED="1516825131757">
<node TEXT="iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT" ID="ID_1951259026" CREATED="1516825133948" MODIFIED="1516827803680">
<font SIZE="8"/>
</node>
<node TEXT="faut-il limitation aux connexions &#xe9;tablies" ID="ID_607894697" CREATED="1516825204915" MODIFIED="1516825236054">
<icon BUILTIN="help"/>
</node>
</node>
<node ID="ID_1772570765" CREATED="1516825138276" MODIFIED="1516827032568"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      redirection de <i>subnet0</i>&#160;vers <i>internet0</i>
    </p>
  </body>
</html>
</richcontent>
<node TEXT="iptables -A FORWARD -i subnet0 -o internet0 -j ACCEPT" ID="ID_1014658106" CREATED="1516825167971" MODIFIED="1516827802888">
<font SIZE="8"/>
</node>
</node>
</node>
<node TEXT="connexions machines clientes" ID="ID_1190622468" CREATED="1516827829624" MODIFIED="1516827841208">
<icon BUILTIN="full-5"/>
<node TEXT="configuration DHCPD" ID="ID_521020084" CREATED="1516825379288" MODIFIED="1516827848922">
<node ID="ID_24088702" CREATED="1516825702113" MODIFIED="1516827884835"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      si pas d&#233;j&#224; actif pour <i>subnet0</i>
    </p>
  </body>
</html>
</richcontent>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_160161290" STARTINCLINATION="978;0;" ENDINCLINATION="761;47;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
</node>
<node TEXT="configuration manuelle de chacune" ID="ID_1158109005" CREATED="1516827850792" MODIFIED="1516827861874">
<node TEXT="interface" ID="ID_1902740149" CREATED="1516828046293" MODIFIED="1516828048871">
<node TEXT="ip addr add {IP_statique_client}/{netmask} dev {intf}" ID="ID_1163742039" CREATED="1516827864040" MODIFIED="1516969089258">
<font SIZE="8"/>
</node>
<node TEXT="ip link set up dev {intf}" ID="ID_1023172249" CREATED="1516827947438" MODIFIED="1516969085387">
<font SIZE="8"/>
</node>
<node TEXT="ip route add default via {IP_routeur} dev {intf}" ID="ID_1674684144" CREATED="1516827951758" MODIFIED="1516969081876">
<font SIZE="8"/>
</node>
</node>
<node TEXT="DNS" ID="ID_1465043538" CREATED="1516828050405" MODIFIED="1516828053077">
<node TEXT="/etc/resolv.conf" ID="ID_1583690583" CREATED="1516828053080" MODIFIED="1516828062959">
<node TEXT="nameserver {IP_serveur_DNS}" ID="ID_1380704918" CREATED="1516828079604" MODIFIED="1516828093060">
<font SIZE="8"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Configuration" POSITION="left" ID="ID_129384384" CREATED="1516824305912" MODIFIED="1516829837967">
<edge COLOR="#0000ff"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="interface" ID="ID_551984622" CREATED="1516828926111" MODIFIED="1516828945890">
<node TEXT="site web local" ID="ID_1022597457" CREATED="1516824325022" MODIFIED="1516828954466"/>
<node TEXT="ligne de commande via SSH" ID="ID_459285736" CREATED="1516828954887" MODIFIED="1516828966719"/>
</node>
<node TEXT="accessible" ID="ID_590255213" CREATED="1516824385919" MODIFIED="1516824389143">
<node TEXT="Ethernet" ID="ID_662463862" CREATED="1516824389149" MODIFIED="1516824392528"/>
<node TEXT="+/- Wifi" ID="ID_963222236" CREATED="1516824392886" MODIFIED="1516824403675">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="options" ID="ID_714997946" CREATED="1516825800485" MODIFIED="1516825803054">
<node TEXT="sens" ID="ID_335867882" CREATED="1516824329768" MODIFIED="1516825808914">
<node TEXT="" ID="ID_71699968" CREATED="1516825815910" MODIFIED="1516825815910">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Wifi -&gt; Ethernet" ID="ID_1775012221" CREATED="1516824333447" MODIFIED="1516824339146"/>
<node TEXT="Ethernet -&gt; Wifi" ID="ID_1820254954" CREATED="1516824341079" MODIFIED="1516824346547"/>
<node TEXT="Wifi &gt; Wifi" ID="ID_480646210" CREATED="1516826316442" MODIFIED="1516826323163">
<node TEXT="si support&#xe9; par carte Wifi" ID="ID_1753088285" CREATED="1516826330265" MODIFIED="1516826336635"/>
</node>
<node TEXT="" ID="ID_40061763" CREATED="1516825815908" MODIFIED="1516825815910">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node ID="ID_1126904319" CREATED="1516825815911" MODIFIED="1520013743445"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      d&#233;termination interfaces <i>internet0</i>&#160;et <i>subnet0</i>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="r&#xe9;seau public" ID="ID_175689784" CREATED="1516825973576" MODIFIED="1516825976906">
<node TEXT="Wifi" ID="ID_1614323801" CREATED="1516825886858" MODIFIED="1516825889740">
<node TEXT="SSID" ID="ID_1772866607" CREATED="1516825897074" MODIFIED="1516825898676"/>
<node TEXT="s&#xe9;curit&#xe9;" ID="ID_1667419070" CREATED="1516826002896" MODIFIED="1516826004905">
<node TEXT="type" ID="ID_244605095" CREATED="1516826006144" MODIFIED="1516826007521">
<node TEXT="aucune" ID="ID_693605936" CREATED="1516826652491" MODIFIED="1516826654045"/>
<node TEXT="WEP" ID="ID_257285475" CREATED="1516826008216" MODIFIED="1516826009257"/>
<node TEXT="WPA" ID="ID_1675997580" CREATED="1516826009407" MODIFIED="1516826016217"/>
<node TEXT="WPA2" ID="ID_8960300" CREATED="1516826016687" MODIFIED="1516826019513">
<node TEXT="PSK" ID="ID_1782806698" CREATED="1516826019855" MODIFIED="1516826022529"/>
<node TEXT="..." ID="ID_418657280" CREATED="1516826022903" MODIFIED="1516826024089"/>
</node>
</node>
<node ID="ID_1150360584" CREATED="1516825851143" MODIFIED="1516825896341"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <i>passphrase</i>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="IP" ID="ID_1078920353" CREATED="1516825977920" MODIFIED="1516825981152">
<node TEXT="statique" ID="ID_879630609" CREATED="1516825981155" MODIFIED="1516825984706"/>
<node TEXT="DHCP" ID="ID_81330781" CREATED="1516825984944" MODIFIED="1516825986794"/>
</node>
</node>
<node TEXT="r&#xe9;seau local" ID="ID_37589095" CREATED="1516826604844" MODIFIED="1516826607406">
<node TEXT="Wifi" ID="ID_78936774" CREATED="1516825886858" MODIFIED="1516825889740">
<node TEXT="SSID" ID="ID_1453187963" CREATED="1516825897074" MODIFIED="1516825898676"/>
<node TEXT="s&#xe9;curit&#xe9;" ID="ID_1772551066" CREATED="1516826002896" MODIFIED="1516826004905">
<node TEXT="type" ID="ID_1427853924" CREATED="1516826006144" MODIFIED="1516826007521">
<node TEXT="aucune" ID="ID_1401293514" CREATED="1516826643699" MODIFIED="1516826646358"/>
<node TEXT="WEP" ID="ID_970872428" CREATED="1516826008216" MODIFIED="1516826009257"/>
<node TEXT="WPA" ID="ID_1189205511" CREATED="1516826009407" MODIFIED="1516826016217"/>
<node TEXT="WPA2" ID="ID_1966244134" CREATED="1516826016687" MODIFIED="1516826019513">
<node TEXT="PSK" ID="ID_1602214332" CREATED="1516826019855" MODIFIED="1516826022529"/>
<node TEXT="..." ID="ID_1042331445" CREATED="1516826022903" MODIFIED="1516826024089"/>
</node>
</node>
<node ID="ID_765349621" CREATED="1516825851143" MODIFIED="1516825896341"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <i>passphrase</i>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="IP" ID="ID_466006639" CREATED="1516825977920" MODIFIED="1516825981152">
<node TEXT="statique" ID="ID_1593016369" CREATED="1516825981155" MODIFIED="1520013778185">
<node TEXT="configuration manuelle des machines clientes" ID="ID_428584211" CREATED="1520013778196" MODIFIED="1520013783388"/>
</node>
<node TEXT="serveur DHCP" ID="ID_1417485840" CREATED="1516825984944" MODIFIED="1516826711845">
<node TEXT="plage d&apos;adresses" ID="ID_1603808664" CREATED="1516826767265" MODIFIED="1516826773817">
<node TEXT="nombre max de machines clientes" ID="ID_109320258" CREATED="1516826773821" MODIFIED="1516826782619"/>
</node>
<node TEXT="masque de sous-r&#xe9;seau" ID="ID_1594219657" CREATED="1516826754186" MODIFIED="1516826758316"/>
<node TEXT="serveurs DNS" ID="ID_659185502" CREATED="1516826703082" MODIFIED="1516826750428"/>
</node>
</node>
</node>
</node>
</node>
<node TEXT="S&#xe9;curit&#xe9;" POSITION="right" ID="ID_1206684547" CREATED="1516826427104" MODIFIED="1516831432634">
<icon BUILTIN="internet_warning"/>
<edge COLOR="#00007c"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="Parefeu" ID="ID_632090203" CREATED="1516828168603" MODIFIED="1516828178107">
<icon BUILTIN="help"/>
<node TEXT="limiter les autres connexions" ID="ID_1304486670" CREATED="1516828179083" MODIFIED="1516828185653"/>
</node>
<node TEXT="interface de configuration" ID="ID_609570577" CREATED="1516828191371" MODIFIED="1516828281437">
<node TEXT="identifiants" ID="ID_602465652" CREATED="1516828240546" MODIFIED="1516828243884">
<node TEXT="un utilisateur du syst&#xe8;me" ID="ID_1054599188" CREATED="1516828244898" MODIFIED="1516830936496">
<icon BUILTIN="help"/>
<node TEXT="avec droits sudo" ID="ID_766825277" CREATED="1516830752969" MODIFIED="1516830760056"/>
<node TEXT="&#xe9;viter de donner droits root au serveur web" ID="ID_905038266" CREATED="1516830902623" MODIFIED="1516830919162"/>
</node>
</node>
<node TEXT="inaccessible depuis le r&#xe9;seau publique" ID="ID_1998737124" CREATED="1516828251234" MODIFIED="1516828267212"/>
<node ID="ID_1797464971" CREATED="1516830768817" MODIFIED="1516830794610"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      http<b>s</b>
    </p>
  </body>
</html>
</richcontent>
<node TEXT="transmission identifiants sensibles" ID="ID_211683128" CREATED="1516830799352" MODIFIED="1516830813313">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1054599188" STARTINCLINATION="49;0;" ENDINCLINATION="49;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
</node>
</node>
</node>
<node TEXT="Programme" POSITION="right" ID="ID_1198485497" CREATED="1516828318529" MODIFIED="1516829845668">
<edge COLOR="#7c007c"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
<node TEXT="langage" ID="ID_135611270" CREATED="1516828322984" MODIFIED="1516828325401">
<node TEXT="Python" ID="ID_672025689" CREATED="1516828325404" MODIFIED="1516828342243">
<node TEXT="&gt;= 3.5" ID="ID_502753597" CREATED="1516828343112" MODIFIED="1516828392066">
<icon BUILTIN="help"/>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      version actuelle dans Debian stable (24/01/18)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="structure" ID="ID_1219886763" CREATED="1516831092364" MODIFIED="1516831095623">
<node TEXT="module" ID="ID_1896589178" CREATED="1516831096844" MODIFIED="1516831105660">
<node TEXT="fonctions pour configuration r&#xe9;seau" ID="ID_1509900714" CREATED="1516831105665" MODIFIED="1516831116463"/>
</node>
<node TEXT="interface avec l&apos;utilisateur" ID="ID_999429098" CREATED="1516831118412" MODIFIED="1516831130396">
<node TEXT="ligne de commande" ID="ID_1495576628" CREATED="1516831133068" MODIFIED="1516831142020">
<node TEXT="via SSH" ID="ID_1006384665" CREATED="1516831142024" MODIFIED="1516831145599"/>
<node TEXT="on commence par &#xe7;a" ID="ID_1311885207" CREATED="1516828967687" MODIFIED="1516829826655" COLOR="#cc0000">
<icon BUILTIN="idea"/>
<icon BUILTIN="yes"/>
<font SIZE="12" BOLD="true"/>
</node>
</node>
<node TEXT="page web" ID="ID_1191537410" CREATED="1516831130401" MODIFIED="1516831132311"/>
</node>
<node TEXT="script pour le serveur web" ID="ID_1327794523" CREATED="1516831161867" MODIFIED="1516831172966">
<node TEXT="solutions" ID="ID_1267970584" CREATED="1516829023678" MODIFIED="1516831196310">
<icon BUILTIN="help"/>
<node TEXT="module http.server" ID="ID_1279518198" CREATED="1516829051622" MODIFIED="1516829063244" LINK="https://docs.python.org/3.5/library/http.server.html"/>
<node TEXT="framework" ID="ID_308175236" CREATED="1516829026026" MODIFIED="1516829071135">
<node TEXT="Django" ID="ID_1429473453" CREATED="1516829077237" MODIFIED="1516829079592">
<node TEXT="lourd !" ID="ID_1036505264" CREATED="1516829098717" MODIFIED="1516829107216"/>
</node>
<node TEXT="Flask" ID="ID_1233045915" CREATED="1516829079837" MODIFIED="1516829093472"/>
<node TEXT="Tornado" ID="ID_617871246" CREATED="1516829093621" MODIFIED="1516829095344"/>
<node TEXT="..." ID="ID_1532811645" CREATED="1516829095605" MODIFIED="1516829097120"/>
</node>
</node>
</node>
</node>
<node TEXT="configuration r&#xe9;seau" ID="ID_1643029117" CREATED="1516828398479" MODIFIED="1516828412767">
<node TEXT="dynamique" ID="ID_1664467980" CREATED="1516828412771" MODIFIED="1516828416759">
<node TEXT="commandes r&#xe9;seau" ID="ID_1609684058" CREATED="1516828416763" MODIFIED="1516828428561"/>
<node TEXT="je pr&#xe9;f&#xe8;re" ID="ID_1412281851" CREATED="1516828474078" MODIFIED="1516828606116">
<icon BUILTIN="bookmark"/>
<node TEXT="plus &quot;joli&quot;" ID="ID_761033356" CREATED="1516828489054" MODIFIED="1516828503656"/>
<node TEXT="ne touche pas &#xe0; la configuration par d&#xe9;faut au d&#xe9;marrage" ID="ID_1813007525" CREATED="1516828503918" MODIFIED="1516828523214">
<node TEXT="garantit l&apos;acc&#xe8;s &#xe0; l&apos;interface de configuration" ID="ID_1563583732" CREATED="1516828523217" MODIFIED="1516828536008"/>
</node>
</node>
</node>
<node TEXT="statique" ID="ID_176292807" CREATED="1516828429575" MODIFIED="1516828432719">
<node TEXT="copie de fichiers de configuration pr&#xe9;-&#xe9;tablis" ID="ID_53645198" CREATED="1516828432722" MODIFIED="1516828455009"/>
<node TEXT="inconv&#xe9;nients" ID="ID_163607985" CREATED="1516828483558" MODIFIED="1516828487649">
<node TEXT="moins souple" ID="ID_1370911105" CREATED="1516828544701" MODIFIED="1516828548144"/>
<node TEXT="probl&#xe8;me si reboot inopin&#xe9; sans avoir pu r&#xe9;tablir la configuration permettant un &#xe9;tat initial sain" ID="ID_1345358192" CREATED="1516872112561" MODIFIED="1516872152986"/>
<node TEXT="quid de iptables ?" ID="ID_842310206" CREATED="1516828549101" MODIFIED="1516828598199">
<node TEXT="ne sera probablement pas stock&#xe9; &quot;en dur&quot;" ID="ID_1891347096" CREATED="1516828567857" MODIFIED="1516828594903"/>
</node>
</node>
</node>
</node>
<node TEXT="configuration du programme" ID="ID_220601944" CREATED="1516830830568" MODIFIED="1516830841792">
<node TEXT="droits d&apos;acc&#xe8;s" ID="ID_722849317" CREATED="1516830841798" MODIFIED="1516830853448">
<node TEXT="utilisateur syst&#xe8;me" ID="ID_1301143236" CREATED="1516830853454" MODIFIED="1516830868136">
<node TEXT="mais pas le mot de passe" ID="ID_1932224692" CREATED="1516830868142" MODIFIED="1516830882796">
<icon BUILTIN="yes"/>
</node>
</node>
</node>
<node TEXT="valeurs par d&#xe9;faut" ID="ID_881036799" CREATED="1516830973078" MODIFIED="1516830978703">
<node TEXT="IP statique du routeur" ID="ID_1970228470" CREATED="1516830978708" MODIFIED="1516830986346"/>
<node TEXT="plage d&apos;adresses IP pour le DHCP" ID="ID_592919006" CREATED="1516830990254" MODIFIED="1516830998924"/>
<node TEXT="masque de sous-r&#xe9;seau" ID="ID_1648621142" CREATED="1516826754186" MODIFIED="1516826758316"/>
<node TEXT="serveurs DNS" ID="ID_1679866491" CREATED="1516826703082" MODIFIED="1516826750428"/>
</node>
<node TEXT="r&#xe9;pertoire d&apos;installation" ID="ID_1208348786" CREATED="1516831059229" MODIFIED="1516831070141">
<node TEXT="script serveur" ID="ID_943477307" CREATED="1516831070146" MODIFIED="1516831074060"/>
<node TEXT="programme" ID="ID_851009598" CREATED="1516831075436" MODIFIED="1516831088234">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="commandes" ID="ID_967838665" CREATED="1516965607324" MODIFIED="1520013666026">
<node TEXT="client dhcp" ID="ID_1278433386" CREATED="1516965614509" MODIFIED="1516965620659">
<node TEXT="dhcpcd" ID="ID_1906525644" CREATED="1516965620664" MODIFIED="1516965628949"/>
<node TEXT="dhclient" ID="ID_542415851" CREATED="1516965630251" MODIFIED="1516965632496"/>
</node>
</node>
</node>
<node TEXT="script d&apos;installation" ID="ID_1041436433" CREATED="1516831260354" MODIFIED="1516831365868">
<icon BUILTIN="help"/>
<node TEXT="v&#xe9;rification des d&#xe9;pendances" ID="ID_187492009" CREATED="1516831268002" MODIFIED="1516831359596" LINK="#ID_1582545784"/>
<node TEXT="utilisateur syst&#xe8;me" ID="ID_1988767400" CREATED="1516831378032" MODIFIED="1516831386188"/>
</node>
<node TEXT="questions" ID="ID_52612243" CREATED="1516828661579" MODIFIED="1516828706142">
<icon BUILTIN="help"/>
<node TEXT="meilleur moyen de faire ex&#xe9;cuter des commandes r&#xe9;seau par root &#xe0; partir de l&apos;interface web" ID="ID_1740978574" CREATED="1516828665287" MODIFIED="1520013696013"/>
<node TEXT="meilleure fa&#xe7;on de g&#xe9;n&#xe9;rer le code HTML" ID="ID_1109869296" CREATED="1516831221010" MODIFIED="1516831234973"/>
</node>
</node>
<node TEXT="r&#xe9;f&#xe9;rences" LOCALIZED_STYLE_REF="defaultstyle.floating" POSITION="right" ID="ID_938054620" CREATED="1516872741661" MODIFIED="1516872822082" HGAP_QUANTITY="-13.49999959766869 pt" VSHIFT_QUANTITY="458.24998634308616 pt">
<hook NAME="FreeNode"/>
<node TEXT="Internet sharing" ID="ID_1798425628" CREATED="1516872761960" MODIFIED="1516872817429" LINK="https://wiki.archlinux.org/index.php/Internet_sharing"/>
<node TEXT="DHCP server" ID="ID_862217026" CREATED="1516872802656" MODIFIED="1516872809883" LINK="https://wiki.archlinux.org/index.php/Dhcpd"/>
<node TEXT="Software access point" ID="ID_1077051338" CREATED="1516872781232" MODIFIED="1516872790469" LINK="https://wiki.archlinux.org/index.php/Software_access_point"/>
<node TEXT="Wifi" ID="ID_524133558" CREATED="1516955594808" MODIFIED="1516955600672" LINK="https://wiki.archlinux.org/index.php/Wireless_network_configuration"/>
</node>
<node TEXT="" POSITION="left" ID="ID_725527844" CREATED="1520013434486" MODIFIED="1520013434500">
<edge COLOR="#007c7c"/>
</node>
</node>
</map>
