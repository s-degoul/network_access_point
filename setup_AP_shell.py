#!/usr/bin/env python3
#-*- coding:utf-8 -*-

""" Setup an access point via shell """

import signal
import config
from APUtils import *


def binary_ask(question, default_answer=""):
    """ Ask to the user a close ended question
    Input: question: str. Question to be asked
           default_answer: str. Default answer: positive ("y"), negative ("n") or not defined ("")
    Output: bool. True (if answer is yes) or False (if answer is no)
    """
    assert default_answer in (
        "y", "n", ""), "Asking a binary question: default_answer must be y, n or empty string"

    answer = ""
    while True:
        answer = input(question + " " +
                       ("[Y/n]" if default_answer == "y" else ("[y/N]" if default_answer == "n" else "[y/n]")) + " ").lower()
        if answer == "":
            answer = default_answer
        if answer in ("y", "n"):
            break
        else:
            print("Please answer \"y\" or \"n\"")
    if answer == "y":
        return True
    return False


def ask_for_positive_int(question, default_answer=1, include_zero=False):
    """ Ask the user for a positive integer
    Input: question: str. Question to be asked
           default_answer: int. Default answer
           include_zero: bool. Should answer "0" be accepted ?
    Output: int
    """
    assert isinstance(default_answer, int) and (
        (include_zero and default_answer >= 0) or (not include_zero and default_answer > 0)), "Asking for a positive int: default answer must be superior to 0"

    number = 0
    while True:
        try:
            number = input(
                question + " (default: " + str(default_answer) + ") ")
            if number == "":
                number = default_answer
                break
            number = int(number)
            if (not include_zero and number > 0) or (include_zero and number >= 0):
                break
            else:
                print("Please enter a positive integer")
        except ValueError as e:
            print("Please enter an integer !")
    return number


def choose_in_list(question, choices):
    """ Ask the user to choose between given choices
    Input: question: str. Question to be asked
           choices: dict or list/tuple. Proposed choices
    Ouput: one item among choices: value in case of input list/tuple, key in case of input dict
    """
    items = {}
    print(question)
    i = 1
    for choice in choices:
        choice_value = str(
            choice) + (": " + str(choices[choice])) if isinstance(choices, dict) else ""
        items[i] = choice
        print(str(i) + ". " + choice_value)
        i += 1
    while True:
        try:
            answer = int(
                input("Enter the number corresponding to your choice: "))
            if answer in items:
                break
            else:
                print("This choice is not available")
        except ValueError as e:
            print("Please enter an integer !")
    return items[answer]


def get_title(title, level):
    """ Render a highlighted title
    Input: title: str. Title to be printed out
           level: int. Level of title
    Output: None
    """
    marks_nb = 10 // level
    return "-" * marks_nb + title + "-" * marks_nb


def create_AP():
    """ Create the access point
    Input: None
    Output: None
    """
    print("Welcome to access point setup software\n\n")

    apu = APUtils()

    #-----------------------------------#
    # Connection to Internet

    print(get_title("Connection to the Internet", 1))

    internet_connection = binary_ask(
        "Do you want to connect to the Internet ?",
        default_answer="y")

    if internet_connection:
        apu.set_intf_internet(choose_in_list(question="Enter the name of the network interface to connect to the Internet",
                                             choices=apu.interfaces))

        # Configure IP address
        print(get_title("Configuration of IP address", 2))
        ip_config = dict(dhcp=binary_ask("Use DHCP ?"), ip_client="",
                         ip_router="", broadcast="")
        if not ip_config["dhcp"]:
            while not APUtils.is_ip_valid(ip_config["ip_client"]):
                ip_config["ip_client"] = input(
                    "- IP address of this machine ? ")
            while not APUtils.is_ip_valid(ip_config["ip_router"]):
                ip_config["ip_router"] = input("- IP address of the router ? ")
            while not APUtils.is_ip_valid(ip_config["broadcast"]):
                ip_config["broadcast"] = input(
                    "- Broadcast address ? (optional) ")
                if ip_config["broadcast"] == "":
                    break

        # If WIFI, connection to WIFI network
        if apu.is_wifi_interface(apu.intf_internet):
            print(get_title("Connection to a WIFI network", 2))
            ssid = input("- name of the WIFI network SSID: ")
            encryption = None
            passphrase = ""
            while encryption not in ("", "wep", "wpa"):
                encryption = input(
                    "- type of encryption ? (wpa or wep, leave blank if none): ").lower()
            if encryption:
                while not passphrase:
                    passphrase = input("- passphrase for encryption ? ")

            apu.connect_wifi(ssid=ssid, encryption=encryption,
                             passphrase=passphrase, **ip_config)
        # Else, connection to Ethernet network
        else:
            print(get_title("Connection to an ethernet network", 2))
            apu.connect_ethernet(**ip_config)

        # Has the connection succeeded ?
        # time.sleep(3)               # Usefull ?
        if not APUtils.is_connected(apu.intf_internet):
            print("Unable to establish a connection of " +
                  apu.intf_internet + " to the Internet. Aborting.")
            # sys.exit(1)
        else:
            print("Connection to the Internet established")

    #-----------------------------------#
    # Connection to the LAN

    print(get_title("Setting the local area network (LAN)", 1))

    apu.set_intf_subnet(choose_in_list(question="Enter the name of the network interface to connect to the local network",
                                       choices={k: v for k, v in apu.interfaces.items() if k != apu.intf_internet}))

    # If WIFI, configure and launch hostapd utility
    if apu.is_wifi_interface(apu.intf_subnet):
        print(get_title("Configuring WIFI access point daemon", 2))
        apu.stop_hostapd()
        apu.set_hostapd_conf(dict(conf_file=config.hostapd_conf_file,
                                  ssid=input(
                                      "- name of the WIFI network SSID: "),
                                  passphrase=input("- passphrase for WPA2 encryption ? ")))
        apu.set_max_client_number(ask_for_positive_int("- max client number ?",
                                                       config.default_subnet_conf["max_client_number"]))
        apu.start_hostapd()

    # Creation of NAT
    if internet_connection:
        print(get_title("Setting up the NAT", 2))
        apu.create_NAT()

    # Manage IP attribution
    print(get_title("Configuring IP addresses attribution", 2))
    subnet_conf = dict(subnet="", netmask="", router_ip="")
    for param in subnet_conf:
        while True:
            subnet_conf[param] = input(
                "- " + param.replace("_", " ") + " address? (default: " + str(config.default_subnet_conf[param]) + ") ")
            if APUtils.is_ip_valid(subnet_conf[param]):
                break
            elif subnet_conf[param] == "":
                subnet_conf[param] = config.default_subnet_conf[param]
                break
            else:
                print("Please give a valid IPv4 address")
    apu.set_subnet_conf(subnet_conf)

    # Launch DHCP server if any
    dhcp_server = binary_ask("Launch a DHCP server for the LAN ?")
    if dhcp_server:
        apu.stop_dhcp_server()
        if not apu.subnet_conf["max_client_number"]:
            apu.set_max_client_number(ask_for_positive_int("- max client number ?",
                                                           config.default_subnet_conf["max_client_number"]))
        dhcp_server_conf = dict(conf_file=config.dhcp_server_conf_file,
                                dns_ip="",
                                min_client_ip="")
        for param in ("dns_ip", "min_client_ip"):
            while True:
                dhcp_server_conf[param] = input(
                    "- " + param.replace("_", " ") + " address? (default: " + str(config.default_dhcp_server_conf[param]) + ") ")
                if APUtils.is_ip_valid(dhcp_server_conf[param])\
                   or (param == "dns_ip"
                       and False not in [APUtils.is_ip_valid(i) for i in [j.strip() for j in dhcp_server_conf[param].split(",")]]):
                    break
                elif dhcp_server_conf[param] == "":
                    dhcp_server_conf[param] = config.default_dhcp_server_conf[param]
                    break
                else:
                    print("Please give a valid IPv4 address")
        apu.set_dhcp_server_conf(dhcp_server_conf)
        apu.start_dhcp_server()
    else:
        print("Run following commands (by root) on client machine to get connection (IFNAME: network interface, IFADDR: desired IP address):")
        print("  ip link set down dev IFNAME")
        print("  ip addr add IFADDR/" +
              str(APUtils.convert_netmask_to_num(apu.subnet_conf["netmask"])) + " dev IFNAME")
        print("  ip link set up dev IFNAME")
        print("  ip route add default via " +
              apu.subnet_conf["router_ip"] + " dev IFNAME")
        print("Add DNS server address(es) in /etc/resolv.conf:   nameserver DNSADDR   (address of your choice or: " +
              str((apu.dhcp_server_conf["dns_ip"] if apu.dhcp_server_conf["dns_ip"] else config.default_dhcp_server_conf["dns_ip"])) + ")")

    print("Done")

    # TODO: callable ?
    def stop(signal, frame):
        if (internet_connection):
            print("Stopping Internet connection")
            apu.disconnect(apu.intf_internet)
        print("Stopping LAN")
        if apu.is_wifi_interface(apu.intf_subnet):
            apu.stop_hostapd()
        apu.stop_dhcp_server()
        apu.delete_NAT()        # TODO: complete the function in APUtils
        APUtils.remove_IP(apu.intf_subnet, dhcp=False, route=False)
        print("Bye!")
    signal.signal(signal.SIGINT, stop)
    signal.pause()


if __name__ == "__main__":
    if os.geteuid() != 0:
        print("This program must be run as root. Aborting.")
        sys.exit(1)
    create_AP()
