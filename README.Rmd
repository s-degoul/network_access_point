# Network access point manager

Software to transform a computer running a Linux system in a network access point: Ethernet -> Wifi or Wifi -> Ethernet

Run: ./setup_AP_shell.py

## Dependencies

- python >= 3.5
- basicshell network management commands:
  - ip
  - iw
- system and service manager systemd
- WPA Supplicant
- DHCP server
- DHCP client

## Files

### setup_AP_shell.py

Main program

Setup an AP via interactive shell interface

### manage_network.py

Set of functions to manage network

### hostapd.conf

Configuration file for hostapd utility

Automatically updated by the program

### etc.systemd.system.dhcpd4@.service

Configuration file of systemd service launching DHCP server for a specific network interface

To be copied in /etc/systemd/system/dhcpd4@service

### caracteristiques.mm

General specifications for the software (mindmap)
